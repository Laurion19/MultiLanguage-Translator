# Javascript Library for multi-language site

Build by OkaCode Community/Ecole241
--Version : 1.0.0 released on 19/12/21:16:30

We are working to better the solution for using it efficiently. Your contribution will be helpful.

![Okalib js](https://raw.githubusercontent.com/klauriondareich/MultiLanguage-Translator/master/logo.png "OkaLib JS")

## WHAT IS OKALIB JS ?

OkaLib js is a javascript library for multi language site or app. The aim is about giving an efficient and easy to integrate library for developers. This has been made with ES6 standards.

## HOW TO USE IT ?

* First step  : 
  Create a *Languages directory*, in the directory create languages JSON files. The name of the file should be the language character. Like this fr.json(for French), en.json(for English) and so forth.
  
* Second step : 
Pay attention with the JSON structure of your files, the simplest it is the better the solution will be integrated.

Example of the JSON Structure : 

 **fr.json**
 
```
{
  "fullname" : "Nom complet",
  "firstname" : "Prénom",
  "lastname" : "Nom",
  "birthday": "Anniversaire"
}
```
 **en.json**
 
```
{
  "fullname" : "Full name",
  "firstname" : "First name",
  "lastname" : "Last Name",
  "birthday": "birthday"
}
```
* Third step : 
